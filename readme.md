# Getting Started
The sample is built using Vite & TypeScript and has the MDRC as only other dependency.

## Installing Dependencies
```
npm install
```
## Running the Sample
```
npm run dev
```

Open http://localhost:4000 in your favorite browser