import { Awaitable, Core, HostAdaptor, Providers, ReferenceData } from "@pictofit/public-types";

interface Resource {
  id : string,
  name : string,
  thumbnail : string,
  organization: string,
  references: Array<string>,
  assets : {
    files : {
      fileName : string,
      getLocation : string
    }[]
  }[]
}

/**
 * Helper function to fetch public resources from the PICTOFiT platform API
 */
function fetchPublicResources(kind : 'GARMENT_2D' | 'SCENE' | 'AVATAR_2D') : Promise<Resource[]> {

  const graphQLQuery = `
  query {
    getStockResources(forFormat:WEB, forKind:${kind}) {
      id,
      name,
      thumbnail,
      organization,
    	references,
      assets {
        files {
          fileName,
          getLocation
        }
      }
    }
  }`;

  return fetch("https://api.platform.pictofit.com/graphql/public/v1", {
    method : 'POST',
    headers: {
      accept: "application/json, multipart/mixed",
    },
    body: JSON.stringify( { query : graphQLQuery })
  })
  .then((res) => res.json()) // conver the result to json
  .then((json) => {
    return json.data.getStockResources; // return only the actual relevant data part of the response
  });
}

/**
 * A generic provider that defines the four methods which are common across the different content providers
 */
class GenericPublicAssetProvider {

  constructor(protected _items : Promise<Resource[]>) {
  }

  public getName(id: ReferenceData): Awaitable<string | undefined> {
    return this._items.then((assets) => {
      const asset = assets.find((asset) => asset.organization === id.organizationId && asset.references.some((x) => x === id.reference));
      if(asset === undefined) {
        return undefined;
      }
      return asset.name;
    })
  }

  public getThumbnail(id: ReferenceData): Awaitable<string | undefined> {
    return this._items.then((assets) => {
      const asset = assets.find((asset) => asset.organization === id.organizationId && asset.references.some((x) => x === id.reference));
      if(asset === undefined) {
        return undefined;
      }
      return asset.thumbnail;
    })
  }

  public getIds(): Awaitable<ReferenceData[]> {
    return this._items.then((assets) => {
     return assets.map((asset) => {
      return {
        reference: asset.references[0],
        organizationId: asset.organization
      }
     })
    });
  }

  public getById(id: ReferenceData): Awaitable<Core.AssetInfo | undefined> {
    return this._items.then((assets) => {
      const asset = assets.find((asset) => asset.organization === id.organizationId && asset.references.some((x) => x === id.reference));
      if(asset === undefined) {
        return undefined;
      }
      return asset.assets[0].files.reduce((init, file) => ({
        ...init,
        [file.fileName] : file.getLocation
      }), {} );
    })
  }
}

/**
 * Provides the garments which should be in the dressing room and their respective assets.
 */
class SimpleGarmentProvider extends GenericPublicAssetProvider implements Providers.GarmentProvider {

  constructor() {
    super(fetchPublicResources('GARMENT_2D'));
  }

  /**
   * Extra method of the GarmentProvider to remove an garment from the dressing room. We ignore this for the sake of simplicity
   */
  removeById(_id: ReferenceData): Awaitable<void> {
    throw new Error("Method not implemented.");
  }

}

/**
 * Provides the avatars which should be available in the dressing room and their respective assets.
 */
class SimpleAvatarProvider extends GenericPublicAssetProvider implements Providers.AvatarProvider {

  constructor() {
    super(fetchPublicResources('AVATAR_2D'))
  }

  /**
   * Extra method of the AvatarProdiver interface to get the gender of an avatar. We ignore this for the sake of simplicity.
   */
  getGender(_id: ReferenceData): Awaitable<Core.Gender | undefined> {
    return 'Male';
  }
}

/**
 * Provides the scenes which should be available in the dressing room and their respective assets.
 */
class SimpleSceneProvider extends GenericPublicAssetProvider implements Providers.SceneProvider {

  constructor() {
    super(fetchPublicResources('SCENE'));
  }
}

/**
 * Here we put together the actual host adaptor. The main components are providers for garments, avatars and scenes.
 * The dressing room has different modes (2D, 2D_Parallax, 3D). We only define providers for the 2D_Parallax mode
 * and also set the dressing room to this mode.
 */
export const simpleHostAdapter : HostAdaptor = {
  garments : {
    "2D_Parallax" : new SimpleGarmentProvider()
  },
  avatars : {
    "2D_Parallax" : new SimpleAvatarProvider()
  },
  scenes : {
    "2D_Parallax" : new SimpleSceneProvider()
  },
  dressingRoom : {
    mode : "2D_Parallax",
    options : {
      enablePhysicsSimulation: true
    }
  },
  logger: {
    debug(...args) {
        console.log(`[DEBUG]`, args)
    },
    err(...args) {
        console.log(`[ERROR]`, args)
    },
    info(...args) {
        console.log(`[INFO ]`, args)
    },
    warn(...args) {
        console.log(`[WARN ]`, args)
    },
  },
  analytics: {
    enableTelemetry: false
  }
}