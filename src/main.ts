import './style.css'
import { simpleHostAdapter } from './host-adapter';
import { AssetReference, Collection, DressingRoomBuilder, GarmentInformationProvider, PictofitBuilder, VirtualStylingBuilder } from '@pictofit/public-types';
import { withAnimations, withCustomLogging, withCustomMannequins } from './customized-host-adapter';
import { ExampleGarmentInformationProvider } from './example-vsc-provider';

// Here we build our own host adaptor.
const hostAdaptor =
  withCustomLogging(
    withAnimations(
      withCustomMannequins(
        simpleHostAdapter
      )
    )
  );

const dressingRoomButton = document.getElementById("open-dressing-room");

const virtualStylingButton = document.getElementById("open-virtual-styling")
const virtualStylingButtonCustom = document.getElementById("open-virtual-styling-custom")

let virtualStylingApi: ReturnType<VirtualStylingBuilder["build"]> | null = null;
let mdrcApi: ReturnType<DressingRoomBuilder["build"]> | null = null;

async function openMDRC() {
  if(mdrcApi === null) {
    // At this point we try to open the MDRC but it was never constructed
    console.error(`MDRC was not constructed!`);
    return;
  }

  // Once the modal dressing room is loaded in, we have the api for it available
  const { api } = await mdrcApi;
  // We show the MDRC as soon as possible
  api.show();
}

async function openVSC(collection: Collection, collectionReference: AssetReference, customProvider?: GarmentInformationProvider) {
  if(virtualStylingApi === null) {
    // At this point we try to open the VSC but it was never constructed
    console.error(`VSC was not constructed!`);
    return;
  }

  // Only at this point we wait for the loading of the component to finish
  const { api } = await virtualStylingApi;
  // Make sure to hide the VSC in case it was opened by another button
  api.hide();
  // Once the virtual styling component is loaded in, we have the api for it available
  api.show(
    Promise.resolve(collection),
    collectionReference,
    {
      // This property is a promise to a provider to allow for asynchronous preloading of assets
      informationProvider: customProvider !== undefined ? Promise.resolve(customProvider) : undefined,
    }
  )
}

const pictoFitEntryPoint = async (builder: PictofitBuilder) => {
  const pictofitApi = builder
    // We do setup a default organization id, which is an alias for the customer id
    .forOrganisation("584bc5e1-1620-48d0-879d-40d32576c33d");

  if (dressingRoomButton) {
    // Construct the MDRC component only if we are interested in it.
    // In this case because the `dressingRoomButton` exists.
    mdrcApi = pictofitApi
      // In this example we want to load the Modal dressing room component
      .asModalDressingRoom()
      // We provider our own host adapter implementation
      .withHostAdaptor(hostAdaptor)
      // This will start to load in the modal dressing room component
      .build();

    // We don't have to await the construction of the MDRC component and can directly
    // register the click handler
    dressingRoomButton.onclick = openMDRC
  }

  if (virtualStylingButton || virtualStylingButtonCustom) {
    const virtualStylingBuilder = pictofitApi
      // We want to use the virtual styling component
      .asVirtualStyling();

    // This will start to load in the virtual styling component
    virtualStylingApi = virtualStylingBuilder.build()

    // We load the collection based on its reference from our central asset platform
    const collection = await virtualStylingBuilder.getCollection(
      "gid://pictofit/collection/demo/everyday-essentials-acc",
      "9a4e90a5-2262-48ca-899e-9216a62dd52b"
    );

    const collectionReference: AssetReference = {
      reference: "gid://pictofit/collection/demo/everyday-essentials-acc",
      organizationId: "9a4e90a5-2262-48ca-899e-9216a62dd52b"
    }

    // If the collection was successfully loaded, we will register a click handler on the button.
    if (collection !== null) {
      // For the normal virtual styling button without custom data providers, we register a click handler
      if (virtualStylingButton) {
        // If the collection exists, we register the click handler for the button
        virtualStylingButton.onclick = () => {
          // Call the VSC open function
          openVSC(collection, collectionReference)
        }

        // For the customized virtual styling button without custom data providers, we register a click handler
        if (virtualStylingButtonCustom) {
          // If the collection exists, we register the click handler for the button
          virtualStylingButtonCustom.onclick = () => {
            // Call the VSC open function
            openVSC(collection, collectionReference, new ExampleGarmentInformationProvider(collection));
          }
        }
      }
    }
  }
}

// This callback is executed when the component API is loaded in correctly.
// The script 
window.onPictofitReady = (builder: PictofitBuilder) => {
  pictoFitEntryPoint(builder);
}
