import {
    AssetReference,
    Awaitable,
    Collection,
    GarmentInformationProvider
} from '@pictofit/public-types';

export class ExampleGarmentInformationProvider implements GarmentInformationProvider {
    constructor(_collection: Collection) {
        // We could use the constructor to preload all information necessary for the collection
    }

    getName(_reference: AssetReference): Awaitable<string | undefined> {
        // We would use the reference to look up the name for the garment in question.
        // The reference string itself looks like the following:
        // `gid://pictofit/sku/<THE_SKU_PROVIDED_TO_PICTOFIT>`
        return "Cool Name";
    }

    getBrand(_reference: AssetReference): Awaitable<string | undefined> {
        return "PICTOFiT";
    }

    getDetailLink(_reference: AssetReference): Awaitable<string | undefined> {
        // We provide a link to the PDP of the garment in question
        return undefined;
    }

    getPrice(_reference: AssetReference): Awaitable<{ price: number; strikePrice?: number; currency: string; } | undefined> {
        // We would provide a price object for the garment
        // This is a fallback method for the pricing methods on the commerce provider
        return undefined;
    }

    preLoad(_items: Array<AssetReference>): Awaitable<void> {
        // We could use the preLoad function to preload all information necessary for the collection
    }
}