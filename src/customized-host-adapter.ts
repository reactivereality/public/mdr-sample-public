import { HostAdaptor } from '@pictofit/public-types';

// The entry point /host-adaptors provides some default implementations for features
// in the host adaptor.
import {
    AvatarProviders,
    defaultAnimationProvider
} from '@pictofit/public-types/host-adaptors';

/**
 * Here we extend a host Adaptor with a custom avatar implementation. This turns
 * on the avatar creator in the modal dressing room. In this implementation the
 * mannequins are stored in the local browser of the user.
 */
export const withCustomMannequins = (hostAdaptor: HostAdaptor): HostAdaptor => {
    return {
        ...hostAdaptor,
        customAvatars: {
            "2D_Parallax": new AvatarProviders.LocalCustomAvatars("2D_Parallax_Custom_Mannequins")
        }
    }
};

/**
 * Here we extend a host Adaptor with an animation provider. This will animate the
 * avatars during tryon. This only works for 2D_Parallax as of writting of this example.
 */
export const withAnimations = (hostAdaptor: HostAdaptor): HostAdaptor => {
    return {
        ...hostAdaptor,
        animations: defaultAnimationProvider
    }
};

/**
 * Here we extend a host Adaptor with a custom log implementation.
 */
export const withCustomLogging = (hostAdaptor: HostAdaptor): HostAdaptor => {
    return {
        ...hostAdaptor,
        logger: {
            debug(...args) {
                console.log(`[DEBUG]`, args)
            },
            err(...args) {
                console.log(`[ERROR]`, args)
            },
            info(...args) {
                console.log(`[INFO ]`, args)
            },
            warn(...args) {
                console.log(`[WARN ]`, args)
            },
        }
    }
};
